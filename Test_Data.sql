/**Find Top 5 Product **/
SELECT TOP 5 product_category_name_english, SUM(CAST(price AS float)) AS Total_Price
FROM product_category_name_translation
INNER JOIN olist_products_dataset
ON product_category_name_translation.product_category_name = olist_products_dataset.product_category_name
INNER JOIN olist_order_items_dataset
ON olist_products_dataset.product_id = olist_order_items_dataset.product_id
GROUP BY product_category_name_english
ORDER BY Total_Price DESC;

/**Find Company Info.**/
WITH 
	o1 AS(
		SELECT COUNT(DISTINCT order_id) AS Total_Order
		FROM olist_orders_dataset
		)
,
	o2 AS(
		SELECT COUNT(DISTINCT customer_unique_id) AS Total_Customer
		FROM olist_customers_dataset
		)
,
	o3 AS(
		SELECT ROUND(SUM(CAST(price AS float) + CAST(freight_value AS float)),2) AS Total_Price
		FROM olist_order_items_dataset
		)

select Total_Order, 
       Total_Customer, 
	   ROUND((CAST(Total_Order AS float)/ CAST(Total_Customer AS float)),2) AS Order_Per_Customer,
	   Total_Price
FROM o1,o2,o3;